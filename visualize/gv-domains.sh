#!/bin/bash

main() {
    local file_gv=${1:-/dev/stdout}

    # get container name from ip
    declare -A container_name
    for container in $(get_containers); do
        ip=$(get_ip $container)
        container_name[$ip]=$container
    done

    print_gv_file $file_gv
}

get_containers() {
    incus ls -f csv -c ns \
        | grep RUNNING \
        | sed 's/,RUNNING//'
}

get_ip() {
    local container=$1
    if [[ -z $container ]]; then
        ip route get 8.8.8.8 | head -1 | sed 's/.*src //' | cut -d' ' -f1
    else
        incus exec $container -- \
              ip route get 8.8.8.8 | head -1 | sed 's/.*src //' | cut -d' ' -f1
    fi
}

print_gv_file() {
    local file_gv=${1:-/dev/stdout}
    cat <<EOF > $file_gv
digraph {
    graph [
        rankdir = LR
        ranksep = 1.5
        concentrate = true
        fontname = system
        fontcolor = brown
    ]

    node [
        shape = box
    ]

    // domain entries of sniproxy
    subgraph sniproxy_domains {
        graph [
            cluster = true
            label = sniproxy
            color = darkgreen
            bgcolor = lightgreen
            style = "rounded, dotted"
        ]

        node [
            fillcolor = lightyellow
            style = "filled"
            fontname = system
            fontcolor = navy
        ]

        $(sniproxy_domains)
    }

    // domain entries of revproxy
    subgraph revproxy_domains {
        graph [
            cluster = true
            color = darkgreen
            bgcolor = lightgreen
            style = "rounded, dotted"
        ]

        node [
            fillcolor = lightyellow
            style = "filled"
            fontname = system
            fontcolor = navy
        ]

        $(revproxy_domains)
    }

    revproxy [
        label = "revproxy"
        margin = 0.2
        shape = box
        peripheries = 2
        style = "filled, rounded"
        color = blue
        fillcolor = deepskyblue
        fontname = system
        fontcolor = navy
    ]

    node [
        shape = record
        style = "filled, rounded"
        color = orange
        fillcolor = yellow
        fontname = system
        fontcolor = navy
    ]

    $(incus_container_nodes)

    edge [
        arrowhead = none
    ]

    $(sniproxy_edges)
    $(revproxy_edges)

    graph [
        labelloc = t
        //labeljust = r
        fontcolor = red
        label = "Domains"
    ]
}
EOF
}

sniproxy_domains() {
    echo
    cat /var/ds/sniproxy/etc/sniproxy.conf \
        | sed -e '1,/table/d' -e '/}/d' -e '/# /d' -e '/^ *$/d' \
        | \
        while read line; do
            domain=$(echo "$line" | cut -d' ' -f1)
            echo "        \"$domain\";"
        done
}

revproxy_domains() {
    echo
    for domain in $(ds @revproxy domains-ls); do
        echo "        \"$domain\";"
    done
}

incus_container_nodes() {
    echo
    for container in $(get_containers); do
        ip=$(get_ip $container)
        cat <<EOF
    $container [ label = "$container|<ip> $ip" ];
EOF
    done
}

sniproxy_edges() {
    echo
    cat /var/ds/sniproxy/etc/sniproxy.conf \
        | sed -e '1,/table/d' -e '/}/d' -e '/# /d' -e '/^ *$/d' \
        | \
        while read line; do
            domain=$(echo "$line" | xargs | cut -d' ' -f1)
            target=$(echo "$line" | xargs | cut -d' ' -f2)
            container=${container_name[$target]}
            [[ -z $container ]] && container=$target
            echo "    \"$domain\" -> \"$container\";"
        done
}

revproxy_edges() {
    echo
    for domain in $(ds @revproxy domains-ls); do
        echo "    revproxy -> \"$domain\";"
    done
}

### call main
main "$@"
