#!/bin/bash

main() {
    local file_gv=${1:-/dev/stdout}

    # get container name from ip
    declare -A container_name
    for container in $(get_containers); do
        ip=$(get_ip $container)
        container_name[$ip]=$container
    done

    print_gv_file $file_gv
}

get_containers() {
    incus ls -f csv -c ns \
        | grep RUNNING \
        | sed 's/,RUNNING//'
}

get_ip() {
    local container=$1
    if [[ -z $container ]]; then
        ip route get 8.8.8.8 | head -1 | sed 's/.*src //' | cut -d' ' -f1
    else
        incus exec $container -- \
              ip route get 8.8.8.8 | head -1 | sed 's/.*src //' | cut -d' ' -f1
    fi
}

print_gv_file() {
    local file_gv=${1:-/dev/stdout}
    cat <<EOF > $file_gv
digraph {
    graph [
        rankdir = LR
        ranksep = 1.5
        concentrate = true
        fontname = system
        fontcolor = brown
    ]

    // all the ports that are accessable in the server
    subgraph ports {
        graph [
            cluster = true
            //label = "Ports"
            style = "rounded, dotted"
        ]
        node [
            fillcolor = lightyellow
            style = "filled"
            fontname = system
            fontcolor = navy
        ]

        // ports that are forwarded to the host itself
        // actually they are the ports that are open in the firewall
        subgraph host_ports {
            graph [
                cluster = true
                style = "rounded"
                color = darkgreen
                bgcolor = lightgreen
                label = "firewall-cmd --list-ports"
            ]

            $(host_port_nodes)
        }

        // ports that are forwarded to the docker containers
        subgraph docker_ports {
            graph [
                cluster = true
                label = "docker ports"
                style = "rounded"
                bgcolor = lightblue
                color = steelblue
            ]

            $(docker_port_nodes)
        }

        // ports that are forwarded to the incus containers
        // with the command: 'incus network forward ...'
        subgraph incus_ports {
            graph [
                cluster = true
                label = "incus network forward"
                style = "rounded"
                bgcolor = pink
                color = orangered
            ]

            $(incus_port_nodes)
        }
    }

    Host [
        label = "Host: $(hostname -f)"
        margin = 0.2
        shape = box
        peripheries = 2
        style = "filled, rounded"
        color = red
        fillcolor = orange
        fontname = system
        fontcolor = navy
    ]

    // docker containers
    subgraph docker_containers {
        graph [
            cluster = true
            label = "Docker"
            style = "rounded"
        ]
        node [
            shape = record
            style = "filled, rounded"
            color = blue
            fillcolor = deepskyblue
            fontname = system
            fontcolor = navy
        ]

        $(docker_container_nodes)
    }
    // incus containers
    subgraph incus_containers {
        graph [
            cluster = true
            label = "Incus"
            style = "rounded"
        ]
        node [
            shape = record
            style = "filled, rounded"
            color = orange
            fillcolor = yellow
            fontname = system
            fontcolor = navy
        ]

        $(incus_container_nodes)
    }

    edge [
        dir = back
        arrowtail = odiamondinvempty
    ]

    $(host_edges)
    $(docker_edges)
    $(incus_edges)

    graph [
        labelloc = t
        //labeljust = r
        fontcolor = red
        label = "Port forwarding"
    ]
}
EOF
}

print_port_node() {
    local protocol=$1
    local port=$2
    cat <<EOF
            "$protocol/$port" [ label = <$protocol <br/> <b>$port</b>> ];
EOF
}

host_port_nodes() {
    echo
    local ports=$(firewall-cmd --list-ports --zone=public)
    for p in $ports; do
        protocol=$(echo $p | cut -d'/' -f2)
        port=$(echo $p | cut -d'/' -f1)
        print_port_node $protocol $port
    done
}

host_edges() {
    echo
    local ports=$(firewall-cmd --list-ports --zone=public)
    for p in $ports; do
        protocol=$(echo $p | cut -d'/' -f2)
        port=$(echo $p | cut -d'/' -f1)
        cat <<EOF
    "$protocol/$port" -> Host;
EOF
    done
}

docker_port_nodes() {
    echo
    docker container ls -a --format "{{.Names}} {{.Ports}}" \
        | grep 0.0.0.0 \
        | sed -e 's/,//g' \
        | tr ' ' "\n" \
        | grep 0.0.0.0 \
        | cut -d: -f2 \
        | \
        while read line; do
            port=$(echo $line | cut -d'-' -f1)
            protocol=$(echo $line | cut -d'/' -f2)
            print_port_node $protocol $port
        done
}

docker_container_nodes() {
    echo
    docker container ls -a --format "{{.Names}} {{.Ports}}" \
        | grep 0.0.0.0 \
        | cut -d' '  -f1 \
        | \
        while read name; do
            echo "        \"$name\";"
        done
}

docker_edges() {
    echo
    docker container ls -a --format "{{.Names}} {{.Ports}}" \
        | grep 0.0.0.0 \
        | sed -e 's/,//g' \
        | \
        while read line; do
            container=$(echo $line | cut -d' ' -f1)
            echo $line | tr ' ' "\n" | grep 0.0.0.0 | cut -d: -f2 | \
                while read line1; do
                    port=$(echo $line1 | cut -d'-' -f1)
                    protocol=$(echo $line1 | cut -d'/' -f2)
                    target=$(echo $line1 | cut -d'>' -f2 | cut -d'/' -f1)
                    [[ $target == $port ]] && target=''
                    cat <<EOF
    "$protocol/$port" -> "$container" [ label = "$target" ];
EOF
                done
        done
}

incus_port_nodes() {
    echo
    get_forward_lines | \
        while read line; do
            eval "$line"
            target_port=${target_port:-$listen_port}
            #echo $description $protocol $listen_port $target_port $target_address
            print_port_node $protocol $listen_port
        done
}

get_forward_lines() {
    local listen_address=$(get_ip)
    incus network forward show incusbr0 $listen_address \
        | sed -e '/^description:/d' \
              -e '/^config:/d' \
              -e '/^ports:/d' \
              -e '/^listen_address:/d' \
              -e '/^location:/d' \
        | tr -d '\n\r' \
        | sed -e 's/- description:/\ndescription:/g' \
        | sed -e 1d \
        | sed -e 's/: /=/g'
}

incus_container_nodes() {
    echo
    for container in $(get_containers); do
        ip=$(get_ip $container)
        cat <<EOF
        $container [ label = "$container|<ip> $ip" ];
EOF
    done
}

incus_edges() {
    echo
    get_forward_lines | \
        while read line; do
            eval "$line"
            #echo $description $protocol $listen_port $target_port $target_address
            container=${container_name[$target_address]}
            cat <<EOF
    "$protocol/$listen_port" -> $container:ip [ label = "$target_port" ];
EOF
        done
}

### call main
main "$@"
