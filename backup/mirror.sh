#!/bin/bash -x

MIRROR_DIR=${1:-/var/bak}
rsync='rsync -avrAX --delete --links --one-file-system'

main() {
    mirror_host
    mirror_incus_containers
    mirror_bbb
}

mirror_host() {
    local mirror=${MIRROR_DIR}/host

    # mirror /root/
    mkdir -p $mirror/root/
    $rsync /root/ $mirror/root/

    # mirror /opt/docker-scripts/
    mkdir -p $mirror/opt/docker-scripts/
    $rsync \
        /opt/docker-scripts/ \
        $mirror/opt/docker-scripts/

    # backup the content of containers
    /var/ds/_scripts/backup.sh

    # mirror /var/ds/
    stop_docker
    mkdir -p $mirror/var/ds/
    $rsync /var/ds/ $mirror/var/ds/
    start_docker
}

stop_docker() {
    local cmd="$* systemctl"
    $cmd stop docker
    $cmd disable docker
    $cmd mask docker
}

start_docker() {
    local cmd="$* systemctl"
    $cmd unmask docker
    $cmd enable docker
    $cmd start docker
}

mirror_incus_containers() {
    local mirror
    local container_list="vclab"
    for container in $container_list ; do
        mirror=$MIRROR_DIR/$container

        # mount container
        mount_root_of_container $container

        # mirror /root/
        mkdir -p $mirror/root/
        $rsync mnt/root/ $mirror/root/

        # mirror /opt/docker-scripts/
        mkdir -p $mirror/opt/docker-scripts/
        $rsync \
            mnt/opt/docker-scripts/ \
            $mirror/opt/docker-scripts/

        # backup the content of the docker containers
        incus exec $container -- /var/ds/_scripts/backup.sh

        # mirror /var/ds/
        stop_docker "incus exec $container --"
        mkdir -p $mirror/var/ds/
        $rsync mnt/var/ds/ $mirror/var/ds/
        start_docker "incus exec $container --"

        # unmount container
        unmount_root_of_container
    done
}

mount_root_of_container() {
    local container=$1
    mkdir -p mnt
    incus file mount $container/ mnt/ &
    MOUNT_PID=$!
    sleep 2
}

unmount_root_of_container() {
    kill -9 $MOUNT_PID
    sleep 2
    rmdir mnt
}

mirror_bbb() {
    local container=bbb
    local mirror=$MIRROR_DIR/$container

    # mount container
    mount_root_of_container $container

    # mirror /root/
    stop_docker "incus exec $container --"
    mkdir -p $mirror/root/
    $rsync mnt/root/ $mirror/root/
    start_docker "incus exec $container --"

    # unmount container
    unmount_root_of_container
}

### call main
main "$@"

