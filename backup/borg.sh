#!/bin/bash

export BORG_PASSPHRASE='XXXXXXXXXXXXXXXX'
export BORG_REPO='storagebox:backup/repo1'

### to initialize the repo run this command (only once)
#borg init -e repokey
#borg key export

dir_to_backup=${1:-/var/bak}

# some helpers and error handling:
info() { printf "\n%s %s\n\n" "$( date )" "$*" >&2; }
trap 'echo $( date ) Backup interrupted >&2; exit 2' INT TERM

info "Starting backup"
borg create \
    --stats \
    --show-rc \
    ::'{hostname}-{now}' \
    $dir_to_backup
backup_exit=$?

info "Pruning repository"
borg prune \
    --list \
    --glob-archives '{hostname}-*' \
    --show-rc \
    --keep-daily    7 \
    --keep-weekly   4 \
    --keep-monthly  6
prune_exit=$?

info "Compacting repository"
borg compact
compact_exit=$?

# use highest exit code as global exit code
global_exit=$(( backup_exit > prune_exit ? backup_exit : prune_exit ))
global_exit=$(( compact_exit > global_exit ? compact_exit : global_exit ))

if [ ${global_exit} -eq 0 ]; then
    info "Backup, Prune, and Compact finished successfully"
elif [ ${global_exit} -eq 1 ]; then
    info "Backup, Prune, and/or Compact finished with warnings"
else
    info "Backup, Prune, and/or Compact finished with errors"
fi

exit ${global_exit}

