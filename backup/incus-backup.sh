#!/bin/bash -x

main() {
    backup_incus_config
    snapshot_containers
    export_containers
}

backup_incus_config() {
    local incus_dir="/var/lib/incus"
    local destination="storagebox:backup/incus"

    incus admin init --dump > $incus_dir/incus.config
    incus --version > $incus_dir/incus.version
    incus list > $incus_dir/incus.instances.list

    rsync \
        -arAX --delete --links --one-file-system --stats -h \
        --exclude disks \
        $incus_dir/ \
        $destination

}

snapshot_containers() {
    # get the day of the week, like 'Monday'
    local day=$(date +%A)

    # get a list of all the container names
    local containers=$(incus list -cn -f csv)

    # make a snapshot for each container
    for container in $containers; do
        incus snapshot create $container $day --reuse
    done
}

export_containers(){
    # mount the storagebox to the directory mnt/
    ssh storagebox mkdir -p backup/incus-containers
    mkdir -p mnt
    sshfs storagebox:backup/incus-containers mnt

    # clean up old export files
    find mnt/ -type f -name "*.tar.xz" -mtime +5 -delete

    # get list of containers to be exported
    # local container_list=$(incus list -cn -f csv)
    local container_list="name1 name2 name3"

    # export containers
    for container in $container_list; do
        incus export $container \
            mnt/$container-$(date +'%Y-%m-%d').tar.xz \
            --optimized-storage
    done

    # unmount
    umount mnt/
    rmdir mnt/
}

# call main
main "$@"

