#!/bin/bash -x

cd $(dirname $0)

MIRROR_DIR=${1:-/var/bak}

# mirror everything to a local dir
./mirror.sh $MIRROR_DIR

# backup the mirror directory to the storagebox
./borg.sh $MIRROR_DIR

# backup the incus setup
./incus-backup.sh
