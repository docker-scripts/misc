#!/bin/bash -x

# update the system
apt update
apt upgrade --yes

# get the latest version of scripts
cd /opt/docker-scripts/
./git.sh pull

# update ds
cd /opt/docker-scripts/ds/
make install

# get the latest version of ubuntu image
docker pull ubuntu:22.04

# run 'ds make' on these apps
app_list="
    sniproxy revproxy mariadb redis
    galene.example.org
    mattermost.example.org
    "
for app in $app_list ; do
    cd /var/ds/$app/
    ds make
done

# run 'ds remake' on these apps
app_list="
    linuxmint
    lxde1    $(: debian-desktop)
    guac.example.org    $(: guacamole)
    toot.example.org    $(: mastodon)
    "
for app in $app_list ; do
    cd /var/ds/$app/
    ds remake
done

# nextcloud
cd /var/ds/cloud.example.org/
ds update

# moodle
cd /var/ds/moodle.example.org/
ds update
ds remake

# discourse
cd /var/ds/talk.example.org/
ds upgrade

# indico
cd /var/ds/events.example.org/
docker pull dockerscripts/indico
ds make

# bookdown
ds /var/ds/misc/books.example.org/
docker pull dockerscripts/bookdown
ds make

# gitea
ds /var/ds/misc/gitea.example.org/
docker pull dockerscripts/gitea
ds make

# linuxmint
cd /var/ds/mate1/
ds build
ds users backup
ds make
ds users restore $(ls backup/*.tgz | tail -1)

# clean up
docker system prune --force

