#!/bin/bash -x

rm /var/ds/*/logs/*.out

cd /var/ds/talk.example.org/
ds backup
find backup -type f -name "*.tar.gz" -mtime +10 -delete

cd /var/ds/guacamole.example.org/
ds backup
find . -type f -name "backup*.tgz" -mtime +10 -delete

cd /var/ds/linuxmint/
ds users backup
find backup/ -type f -name "*.tgz" -mtime +10 -delete

cd /var/ds/moodle.example.org/
ds backup
find . -type f -name "backup*.tgz" -mtime +10 -delete

# list all the backups
find /var/ds/ -name '*.tgz' | xargs ls -lh --color=yes

